# Experiment
This notebook is part of the [Nix tutorial](https://nix-tutorial.gitlabpages.inria.fr/nix-tutorial/).
It shows how to use Nix environments with [R Markdown](https://rmarkdown.rstudio.com/).

The experiment just draws a figure with [tidyverse](https://www.tidyverse.org/) and [viridis](https://cran.r-project.org/web/packages/viridis/vignettes/intro-to-viridis.html).
It is inspired from an [example](http://r-statistics.co/Top50-Ggplot2-Visualizations-MasterList-R-Code.html) done by Selva Prabhakaran.

```{r include=FALSE}
library(tidyverse)
library(viridis)
```

```{r}
theme_set(theme_bw())
g = ggplot(mpg, aes(displ)) + scale_fill_viridis(discrete=TRUE)
g + geom_histogram(aes(fill=class),
                   binwidth = .1,
                   col="black",
                   size=.1) +  # change binwidth
  labs(title="Histogram with Auto Binning",
       subtitle="Engine Displacement across Vehicle Classes")

g + geom_histogram(aes(fill=class),
                   bins=5,
                   col="black",
                   size=.1) +   # change number of bins
  labs(title="Histogram with Fixed Bins",
       subtitle="Engine Displacement across Vehicle Classes")

```
