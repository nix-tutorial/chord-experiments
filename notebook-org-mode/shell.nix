{
  pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/4fe8d07066f6ea82cda2b0c9ae7aee59b2d241b3.tar.gz";
    sha256 = "sha256:06jzngg5jm1f81sc4xfskvvgjy5bblz51xpl788mnps1wrkykfhp";
  }) {}
}:

let
  emacs_base = pkgs.emacs;
  emacsWithPackages = (pkgs.emacsPackagesFor emacs_base).emacsWithPackages;

  self = rec {
    my_emacs = emacsWithPackages (epkgs: with epkgs; [
      org
      org-ref
    ]);

    shell = pkgs.mkShell rec {
      name = "emacs-shell";
      buildInputs = [
        my_emacs
      ];
    };
  };
in
  self.shell
